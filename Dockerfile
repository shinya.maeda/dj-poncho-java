# Use the official OpenJDK base image for compilation
FROM maven AS build

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the Maven project file
COPY pom.xml .

# Copy the source code
COPY src src

# Compile the application
# RUN javac -d target/classes src/main/java/org/example/Main.java
RUN mvn package

# # Package the application as a JAR file
# RUN jar cvfe target/my-app.jar org.example.Main -C target/classes .

# # Use the official OpenJDK base image for the runtime
# FROM openjdk:11-jre-slim

# # Set the working directory in the container
# WORKDIR /usr/src/app

# # Copy the compiled JAR file from the build stage
# COPY --from=build /usr/src/app/target/my-app.jar .

# Specify the command to run on container start
CMD ["java", "-jar", "target/dj-poncho-1.0-SNAPSHOT.jar"]
