# dj-poncho-java

One of many important contributions to keep business running smoothly.

## Usage

Run this application using `bin/start`.

## Testing

Review `.gitlab-ci.yml` for computing known exploited vulnerabilities detected during dependency scanning.
