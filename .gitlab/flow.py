AUTH_TOKEN = "Bearer " + os.environ['GL_AUTH_TOKEN']
OBS_PROJECT_PAT = os.environ['GL_OBS_TOKEN']
OBS_PROJECT_ID = 55834466
ERROR_RATE_THRESHOLD = 0

def check_status(resp, expected_status_code=200):
    if resp.status_code != expected_status_code:
        print("Unexpected HTTP status code", resp.status_code)
        fail("Unexpected HTTP status code", resp.status_code)


def check_content_type(resp, expected_content_type="application/json"):
    ct = resp.header.get("Content-Type", [])
    if len(ct) == 0 or not ct[0].startswith(expected_content_type):
        print("Unexpected Content-Type", ct)
        fail("Unexpected Content-Type", ct)

def get_project(w, project_id):
    resp = w.http.do(
        url="%s/api/v4/projects/%d?title=autoflow" % (w.vars.gitlab_url, project_id),
        header={
            "Accept": "application/json",
            "Authorization": AUTH_TOKEN,
        },
    )
    check_status(resp)
    check_content_type(resp)
    return json.decode(str(resp.body))

def create_issue(w, project_id, title, description, assignee_id):
    base_url = "{endpoint}/api/v4/projects/{project_id}/issues"
    url = base_url.format(endpoint=w.vars.gitlab_url, project_id=project_id)
    print("url: " + url)
    json_body = {
        "title": title,
        "confidential": "true",
        "assignee_id": assignee_id,
        "description": description,
        "issue_type": "incident",
    }
    resp = w.http.do(
        url=url,
        method='POST',
        header={
            "Accept": "application/json",
            "Authorization": AUTH_TOKEN,
            "Content-Type": "application/json",
        },
        body=bytes(json.encode(json_body))
    )
    # By some reason, my GDK returns 500 but successfully created an issue.
    # check_status(resp)
    check_content_type(resp)
    return json.decode(str(resp.body))

def get_deployments(w, project_id):
    base_url = "{endpoint}/api/v4/projects/{project_id}/deployments?environment=production&status=success&order_by=finished_at&sort=desc&per_page=2"
    url = base_url.format(endpoint=w.vars.gitlab_url, project_id=project_id)
    print("url: " + url)
    resp = w.http.do(
        url=url,
        header={
            "Accept": "application/json",
            "Authorization": AUTH_TOKEN,
        }
    )
    check_status(resp)
    check_content_type(resp)
    return json.decode(str(resp.body))

def retry_job(w, project_id, job_id):
    base_url = "{endpoint}/api/v4/projects/{project_id}/jobs/{job_id}/retry"
    url = base_url.format(endpoint=w.vars.gitlab_url, project_id=project_id, job_id=job_id)
    print("url: " + url)
    resp = w.http.do(
        url=url,
        method="POST",
        header={
            "Accept": "application/json",
            "Authorization": AUTH_TOKEN,
        }
    )
    check_status(resp, expected_status_code=201)
    check_content_type(resp)
    return json.decode(str(resp.body))

def get_metric(w, project_id):
    metric_name = "errors_counter"
    metric_type = "Sum"
    period = "1h"
    resp = w.http.do(
        url="https://observe.gitlab.com/v3/query/%s/metrics/search?mname=%s&mtype=%s&period=%s" % (str(project_id), metric_name, metric_type, period),
        header={
            "Accept": "application/json",
            "PRIVATE-TOKEN": OBS_PROJECT_PAT,
        },
    )
    check_status(resp)
    check_content_type(resp)
    return json.decode(str(resp.body))

def auto_rollback(w, ev):
    print("auto_rollback started!")

    project_id = ev["data"]["project_id"]
    print("project_id: " + str(project_id))
    issue = create_issue(w,
        project_id,
        "Attention: " + ev["data"]["deployment_context_title"] + " was rolled back",
        "The following change was **rolled back** due to the high error rate on production environment.\n\n" + ev["data"]["deployment_context_description"],
        ev["data"]["user_id"]
    )
    print("issue: " + str(issue))
    deployments = get_deployments(w, project_id)
    print("latest deployment ID: " + str(deployments[0]["id"]) + " iid: " + str(deployments[0]["iid"]))
    previous_deployment = deployments[1]
    print("previous deployment ID: " + str(previous_deployment["id"]) + " iid: " + str(previous_deployment["iid"]))
    # print("previous_deployment: " + str(previous_deployment))
    print("Rolling back to " + previous_deployment["sha"] + " on " + previous_deployment["ref"])
    previous_deploy_job_id = previous_deployment["deployable"]["id"]
    print("previous_deploy_job_id: " + str(previous_deploy_job_id))
    retried_job = retry_job(w, project_id, previous_deploy_job_id)
    print("retried_job: " + str(retried_job))

def handle_deployment_finished(w, ev):
    # project = get_project(w, 111)
    is_rollback = ev["data"]["is_rollback"]

    if is_rollback == True:
        print("Currently we turn off this auto-rollback feature for rollback jobs so that it won't be eternal loop.")
        return

    print("Got deployment finished event " + str(ev))

    for _ in range(6 * 20): # 20 minutes
        w.sleep(10*time.second)

        metric = get_metric(w, OBS_PROJECT_ID)
        print("Got metric: " + str(metric))

        error_count = 0

        if len(metric["results"]) > 0:
            error_count = metric["results"][0]["values"][0][-1]
        
        if error_count >= ERROR_RATE_THRESHOLD:
            auto_rollback(w, ev)
            break
            

on_event(
    type="com.gitlab.events.deployment_finished",
    handler=handle_deployment_finished,
)
